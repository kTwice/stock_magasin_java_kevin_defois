# stock_magasin_java_kevin_defois

tout d'abord lancer la commande du docker-compose pour build la base de donnée mysql :
```
docker-compose up
```

lorsque le docker est lancé, vous pouvez run l'app principal : 
com.stock.magasin.java.kevin.stock_java.StockJavaApplication

Ensuite il vous faudra ouvrir un navigateur et dans l'url :
```http://localhost:8080/api/stock/```

suivie de la route de votre choix en fonction du retour souhaité :
http://localhost:8080/api/stock/article : pour visionner les articles
http://localhost:8080/api/stock/order : pour visionner les commandes


Pour tester rapidement le get article et order vous pouvez faire ces requêtes en bdd :
```
insert into article value (0.7, 1, 'banane');
insert into article value (0.5, 2, 'pomme');
insert into order_store value (1.2, 1);
insert into order_article value (1,1,1,1);
insert into order_article value (2,2,1,1);
```

Pour creér des articles utiliser la même route que pour le get all mais en requête de type post
par exemple utiliser se type de json
```
{
"nom": "pomme",
"prix": 0.4
}
```

ou bien importer le fichier json 'java_stock_exo.postman_collection' dans postman et toute les routes avec les
json d'exemple y sont noté.