package com.stock.magasin.java.kevin.stock_java.repositories;

import com.stock.magasin.java.kevin.stock_java.models.Article;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface ArticleRepository extends JpaRepository<Article, Long> {
    @Query(value = "select id from article order by id desc limit 1", nativeQuery = true)
    Long getLastId();

}
