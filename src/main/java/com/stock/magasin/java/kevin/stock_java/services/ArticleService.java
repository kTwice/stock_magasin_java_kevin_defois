package com.stock.magasin.java.kevin.stock_java.services;

import com.stock.magasin.java.kevin.stock_java.dto.ArticleDto;
import com.stock.magasin.java.kevin.stock_java.models.Article;
import com.stock.magasin.java.kevin.stock_java.models.OrderArticle;
import com.stock.magasin.java.kevin.stock_java.repositories.ArticleRepository;
import com.stock.magasin.java.kevin.stock_java.repositories.OrderArticleRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ArticleService {
    private final ArticleRepository articleRepository;
    private final OrderArticleRepository orderArticleRepository;

    public List<Article> getAllArticle() {
        return articleRepository.findAll();
    }

    public Optional<Article> getArticleById(long id) {
        return articleRepository.findById(id);
    }

    public void addArticle(ArticleDto articleDto) {
        Article article = new Article();
        article.setId(this.checkId());
        article.setName(articleDto.getName());
        article.setPrice(articleDto.getPrice());
        article.setOrderArticles(this.getOrderArticle(articleDto));
        articleRepository.save(article);
    }

    public void deleteArticle(long id) {
        articleRepository.deleteById(id);
    }

//    public Article dtoToModel(ArticleDto articleDto) {
//        Article article = new Article();
//        article.setId(this.checkId());
//        article.setName(articleDto.getName());
//        article.setPrice(articleDto.getPrice());
//        article.setOrderArticles(this.getOrderArticle(articleDto));
//        return article;
//    }

    private List<OrderArticle> getOrderArticle(ArticleDto articleDto) {
        if (articleDto.getId() == null) {
            return null;
        } else {
            return orderArticleRepository.getAllByArticleId(articleDto.getId());
        }
    }

    private Long checkId() {
        Long idToSave = articleRepository.getLastId();
        if (idToSave == null) {
            return 1L;
        } else {
            return idToSave+1;
        }
    }
}
