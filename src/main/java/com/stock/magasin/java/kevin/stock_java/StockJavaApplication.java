package com.stock.magasin.java.kevin.stock_java;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

@SpringBootApplication(exclude = SecurityAutoConfiguration.class)
public class StockJavaApplication {

	public static void main(String[] args) {
		SpringApplication.run(StockJavaApplication.class, args);
	}

}
