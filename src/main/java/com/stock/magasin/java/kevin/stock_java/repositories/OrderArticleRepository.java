package com.stock.magasin.java.kevin.stock_java.repositories;

import com.stock.magasin.java.kevin.stock_java.models.OrderArticle;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface OrderArticleRepository extends JpaRepository<OrderArticle, Long> {

    @Query(value = "select * from order_article where order_store_id = ?1"
            ,nativeQuery = true)
    List<OrderArticle> getAllByOrderId(long id);

    @Query(value = "select * from order_article where article_id = ?1"
            ,nativeQuery = true)
    List<OrderArticle> getAllByArticleId(long id);

    @Query(value = "select id from order_article order by id desc limit 1", nativeQuery = true)
    Long getLastId();
}
