package com.stock.magasin.java.kevin.stock_java.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ArticleDto {

    private Long id;

    @JsonProperty("nom")
    private String name;

    @JsonProperty("prix")
    private Float price;

    @JsonProperty("quantité")
    private Integer quantity;
}
