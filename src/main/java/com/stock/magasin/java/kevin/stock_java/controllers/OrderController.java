package com.stock.magasin.java.kevin.stock_java.controllers;

import com.stock.magasin.java.kevin.stock_java.dto.OrderDto;
import com.stock.magasin.java.kevin.stock_java.models.Article;
import com.stock.magasin.java.kevin.stock_java.models.Order;
import com.stock.magasin.java.kevin.stock_java.models.OrderArticle;
import com.stock.magasin.java.kevin.stock_java.services.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/api/stock/order", produces = APPLICATION_JSON_VALUE)
public class OrderController {
    private final OrderService orderService;
    private final ArticleController articleController;

    @GetMapping
    public ResponseEntity<List<OrderDto>> searchOrders() {
        List<Order> orders = orderService.getAllOrders();
        List<OrderDto> orderDtoList = orders.stream().map(this::mapToDo).toList();
        return ResponseEntity.status(HttpStatus.OK).body(orderDtoList);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Optional<OrderDto>> getOneOrderById(@PathVariable("id") final long id) {
        Optional<Order> order = orderService.getOrderById(id);
        Optional<OrderDto> orderDto = order.map(this::mapToDo);
        return ResponseEntity.status(HttpStatus.OK).body(orderDto);
    }

    @PostMapping
    public ResponseEntity<OrderDto> createOrder(@RequestBody OrderDto orderDto) {
        orderService.addOrder(orderDto);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    public OrderDto mapToDo(Order order) {
        List<OrderArticle> orderArticles = order.getOrderArticles();
        List<Article> articles = orderArticles.stream().map(OrderArticle::getArticle).toList();
        return new OrderDto(
                order.getId(),
                articles.stream().map(articleController::mapToDo).toList(),
                order.getPrice()
        );
    }
}
