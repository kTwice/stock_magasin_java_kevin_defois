package com.stock.magasin.java.kevin.stock_java.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class OrderDto {
    @JsonProperty("id")
    private Long id;

    @JsonProperty("articles")
    private List<ArticleDto> articleDtoList;

    @JsonProperty("somme")
    private Float price;
}
