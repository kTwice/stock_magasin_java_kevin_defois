package com.stock.magasin.java.kevin.stock_java.repositories;

import com.stock.magasin.java.kevin.stock_java.models.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface OrderRepository extends JpaRepository<Order, Long> {
    @Query(value = "select id from order_store order by id desc limit 1", nativeQuery = true)
    Long getLastId();
}
