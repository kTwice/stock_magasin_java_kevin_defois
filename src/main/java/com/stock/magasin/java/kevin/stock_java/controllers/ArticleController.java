package com.stock.magasin.java.kevin.stock_java.controllers;

import com.stock.magasin.java.kevin.stock_java.dto.ArticleDto;
import com.stock.magasin.java.kevin.stock_java.models.Article;
import com.stock.magasin.java.kevin.stock_java.models.OrderArticle;
import com.stock.magasin.java.kevin.stock_java.services.ArticleService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/api/stock/article", produces = APPLICATION_JSON_VALUE)
public class ArticleController {

    private final ArticleService articleService;

    @GetMapping
    public ResponseEntity<List<ArticleDto>> searchArticles() {
        List<Article> articles = articleService.getAllArticle();
        List<ArticleDto> articleDtoList = articles.stream().map(this::mapToDo).toList();
        return ResponseEntity.status(HttpStatus.OK).body(articleDtoList);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Optional<ArticleDto>> getOneArticleById(@PathVariable("id") final long id) {
        Optional<Article> article = articleService.getArticleById(id);
        Optional<ArticleDto> articleDto = article.map(this::mapToDo);
        return ResponseEntity.status(HttpStatus.OK).body(articleDto);
    }

    @PostMapping
    public ResponseEntity<ArticleDto> createArticle(@RequestBody ArticleDto articleDto) {
        articleService.addArticle(articleDto);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<ArticleDto> deleteArticle(@PathVariable("id") final long id) {
        articleService.deleteArticle(id);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    public ArticleDto mapToDo(Article article) {
        return new ArticleDto(
                article.getId(),
                article.getName(),
                article.getPrice(),
                null
        );
    }
}
