package com.stock.magasin.java.kevin.stock_java.services;

import com.stock.magasin.java.kevin.stock_java.dto.ArticleDto;
import com.stock.magasin.java.kevin.stock_java.dto.OrderDto;
import com.stock.magasin.java.kevin.stock_java.models.Order;
import com.stock.magasin.java.kevin.stock_java.models.OrderArticle;
import com.stock.magasin.java.kevin.stock_java.repositories.OrderArticleRepository;
import com.stock.magasin.java.kevin.stock_java.repositories.OrderRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class OrderService {

    private final OrderRepository orderRepository;
    private final OrderArticleRepository orderArticleRepository;
    private final ArticleService articleService;

    public List<Order> getAllOrders() {
        return orderRepository.findAll();
    }

    public Optional<Order> getOrderById(long id) {
        return orderRepository.findById(id);
    }

    public void addOrder(OrderDto orderDto) {
        Order orderToSave = new Order();
        orderToSave.setId(this.checkId());
        orderToSave.setPrice(this.calculatePrice(orderDto));
        orderRepository.save(orderToSave);
        this.saveArticleOrder(orderDto);
        orderToSave.setOrderArticles(this.getOrderArticle(orderDto));
        orderRepository.save(orderToSave);
    }

    private void saveArticleOrder(OrderDto orderDto) {
        for (ArticleDto articleDto : orderDto.getArticleDtoList()) {
            OrderArticle orderArticle = new OrderArticle();
            Long id = orderArticleRepository.getLastId();
            if (id == null) {
                orderArticle.setId(1L);
            } else {
                orderArticle.setId(id+1);
            }
            orderArticle.setOrder(this.dtoToModel(orderDto));
            if (articleDto.getId() != null && articleService.getArticleById(articleDto.getId()).isPresent()) {
                orderArticle.setArticle(articleService.getArticleById(articleDto.getId()).get());
            }
            orderArticle.setQuantity(articleDto.getQuantity());
            orderArticleRepository.save(orderArticle);
        }
    }

    private List<OrderArticle> getOrderArticle(OrderDto orderDto) {
        if (orderDto.getId() == null) {
            return null;
        } else {
            return orderArticleRepository.getAllByOrderId(orderDto.getId());
        }
    }

    private float calculatePrice(OrderDto orderDto) {
        List<Float> priceList = orderDto.getArticleDtoList().stream().map(ArticleDto::getPrice).toList();
        List<Integer> quantity = orderDto.getArticleDtoList().stream().map(ArticleDto::getQuantity).toList();
        float addition = 0;
        Integer i = 0;
        for (float price: priceList) {
            float multiplication = price * quantity.get(i);
            addition+=multiplication;
            i++;
        }
        return roundAvoid(addition, 2);
    }

    public Order dtoToModel(OrderDto orderDto) {
        Order order = new Order();
        Long id = orderRepository.getLastId();
        if (getOrderById(id).isPresent()) {
            order = getOrderById(id).get();
            order.setOrderArticles(this.getOrderArticle(orderDto));
        }
        return order;
    }

    public static Float roundAvoid(Float value, int places) {
        double scale = Math.pow(10, places);
        return (float) (Math.round(value * scale) / scale);
    }

    private Long checkId() {
        Long idToSave = orderRepository.getLastId();
        if (idToSave == null) {
            return 1L;
        } else {
            return idToSave+1;
        }
    }
}
